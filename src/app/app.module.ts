import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContentComponent } from './layout/content/content.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import {SharedModule} from './shared/shared.module';
import {CoreModule} from './core/core.module';
import { ContentHeaderComponent } from './layout/content-header/content-header.component';
import { LandingContentComponent } from './layout/landing-content/landing-content.component';
import { LandingHeaderComponent } from './layout/landing-header/landing-header.component';
import { LandingFooterComponent } from './layout/landing-footer/landing-footer.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    FooterComponent,
    HeaderComponent,
    ContentHeaderComponent,
    LandingContentComponent,
    LandingHeaderComponent,
    LandingFooterComponent
  ],
  imports: [
    SharedModule,
    CoreModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
