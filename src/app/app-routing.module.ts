import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ContentComponent} from './layout/content/content.component';
import {ContentHeaderComponent} from './layout/content-header/content-header.component';
import {LandingContentComponent} from './layout/landing-content/landing-content.component';
import {LoginGuard} from './core/guard/login.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'product',
        loadChildren: () =>
          import('./modules/product/product.module').then(m => m.ProductModule),
        component: ContentComponent
      },
      {
        path: 'auth',
        loadChildren: () =>
          import('./modules/auth/auth.module').then(m => m.AuthModule),
        component: ContentComponent
      },
      {
        path: 'home',
        loadChildren: () =>
          import('./modules/home/home.module').then(m => m.HomeModule),
        component: LandingContentComponent
      },
      {
        path: 'business',
        loadChildren: () =>
          import('./modules/business/business.module').then(m => m.BusinessModule),
        component: LandingContentComponent
      },
      {
        path: 'about',
        loadChildren: () =>
          import('./modules/about/about.module').then(m => m.AboutModule),
        component: LandingContentComponent
      },
      {
        path: 'contacts',
        loadChildren: () =>
          import('./modules/contacts/contacts.module').then(m => m.ContactsModule),
        component: LandingContentComponent
      },
      {
        path: 'confirmation',
        loadChildren: () =>
          import('./modules/confirmation/confirmation.module').then(m => m.ConfirmationModule),
        component: ContentComponent
      },
      {
        path: 'decision',
        loadChildren: () =>
          import('./modules/decision/decision.module').then(m => m.DecisionModule),
        component: ContentComponent
      },
      {
        path: 'orders',
        loadChildren: () =>
          import('./modules/orders/orders.module').then(m => m.OrdersModule),
        component: ContentComponent,
        canActivate: [LoginGuard]
      },
      {
        path: 'payment',
        loadChildren: () =>
          import('./modules/payment/payment.module').then(m => m.PaymentModule),
        component: ContentComponent,
        // canActivate: [LoginGuard]
      },
      {
        path: 'installment',
        loadChildren: () =>
          import('./modules/installment/installment.module').then(m => m.InstallmentModule),
        component: ContentHeaderComponent,
        canActivate: [LoginGuard]
      },
      {
        path: 'settings',
        loadChildren: () =>
          import('./modules/settings/settings.module').then(m => m.SettingsModule),
        component: ContentComponent,
        canActivate: [LoginGuard]
      },
      {
        path: 'confidentiality',
        loadChildren: () =>
          import('./modules/confidentiality/confidentiality.module').then(m => m.ConfidentialityModule),
        component: ContentComponent
      },
      {
        path: 'contract',
        loadChildren: () =>
          import('./modules/contract/contract.module').then(m => m.ContractModule),
        component: ContentComponent
      },
      {
        path: 'documents',
        loadChildren: () =>
          import('./modules/documents/documents.module').then(m => m.DocumentsModule),
          component: ContentComponent
      },
      {
        path: 'paybox',
        loadChildren: () =>
          import('./modules/paybox/paybox.module').then(m => m.PayboxModule),
        component: ContentComponent
      }
    ]
  },
  { path: '**', redirectTo: '/auth/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
