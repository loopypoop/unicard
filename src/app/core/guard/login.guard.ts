import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../../shared/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {


  constructor(private router: Router,
              private authService: AuthService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean|UrlTree {
    const url: string = state.url;

    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean|UrlTree {
    // Store the attempted URL for redirecting
    this.authService.redirectUrl = url;

    if (localStorage.getItem('userId') === null || !localStorage.getItem('userId')) {
      this.router.navigateByUrl('/auth/login');
      return false;
    }
    return true;
  }
}
