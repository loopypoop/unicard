import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessComponent } from './page/business.component';
import {BusinessBannerComponent} from './page/business-banner/business-banner.component';
import {CarouselComponent} from './page/carousel/carousel.component';
import {RequestBannerComponent} from './page/request-banner/request-banner.component';
import {SharedModule} from '../../shared/shared.module';
import {BusinessRoutingModule} from './business.routing';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [BusinessComponent, BusinessBannerComponent, CarouselComponent, RequestBannerComponent],
    imports: [
        CommonModule,
        SharedModule,
        BusinessRoutingModule,
        FormsModule
    ]
})
export class BusinessModule { }
