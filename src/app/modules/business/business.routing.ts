import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BusinessComponent} from './page/business.component';

export const routes: Routes = [
  {
    path: '',
    component: BusinessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessRoutingModule {}
