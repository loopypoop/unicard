import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpService} from '../../../../shared/service/http.service';

@Component({
  selector: 'app-request-banner',
  templateUrl: './request-banner.component.html',
  styleUrls: ['./request-banner.component.scss'],
  styles: [`
    .dark-modal .modal-content {
      background-color: transparent;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
    .light-blue-backdrop {
      background-color: #5cb3fd;
    }
  `]
})
export class RequestBannerComponent implements OnInit {

  fullName: string;
  email: string;
  phone: string;
  bin: string;
  companyName: string;
  item: string;
  town: string;
  error: string;

  townList = ['Город', 'Алматы', 'Нур-Султан', 'Шымкент', 'Тараз', 'Павлодар', 'Караганды'];
  itemList = ['Категория товаров', 'Одежда', 'Красота и здоровье', 'Детские товары', 'Ремонт/Строительство',
    'Образование', 'Продукты питания', 'Ювелирные изделия', 'Флористика', 'Электроника', 'Товары для дома'];


  constructor(private modalService: NgbModal,
              private httpService: HttpService) {}

  ngOnInit(): void {
    this.item = this.itemList[0];
    this.town = this.townList[0];
  }

  openVerticallyCentered(content): void {
    this.modalService.open(content, { centered: true });
  }

  clear() {
    this.fullName = null;
    this.email = null;
    this.town = this.townList[0];
    this.bin = null;
    this.companyName = null;
    this.item = this.itemList[0];
    this.phone = null;
    this.error = null;
  }

  openSuccessModal(content, modal): void {
    if (!this.fullName || !this.email || !this.phone || !this.bin ||
      !this.companyName || this.item === 'Категория товаров' || this.town === 'Город') {
      this.error = 'Заполните все поля!';
    } else {
      const data = {
        'FIO': this.fullName,
        'Email': this.email,
        'PhoneNumber': '7' + this.phone,
        'Name': this.companyName,
        'BIN': 'BIN' + this.bin,
        'City': this.town,
        'Type': this.item
      };
      this.httpService.businessApplication(data).subscribe(res => {
        console.log('res', res);
        if (res.Status == 0) {
          modal.dismiss();
          this.modalService.open(content, {size: 'sm', centered: true});
        } else if (res.Status == 1) {
          this.error = res.StatusDesc;
        }
        this.clear();
      });
    }
  }

  isDigit(): void {
    document.getElementById("phone").addEventListener("keypress", function (evt) {
      if (evt.which < 48 || evt.which > 57)
      {
        evt.preventDefault();
      }
    });
  }
}
