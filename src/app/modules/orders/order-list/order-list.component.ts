import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../../shared/service/product.service';
import {ProductDTO} from '../../../shared/models/productDTO';
import {User} from '../../../shared/models/user';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

  product: any;
  backProducts: ProductDTO[] = [];
  payProgress = 3;
  activeProducts: ProductDTO[] = [];
  closedProducts: ProductDTO[] = [];
  user: User;
  userPhone = JSON.parse(localStorage.getItem('userPhone'));

  constructor(private productService: ProductService) {
    if (localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
  }

  ngOnInit(): void {
    this.productService.getProductList(this.userPhone).subscribe(res => {
      if (res.Status === 0) {
        if (res.Products.length >= 7) {
          for (let i = 0; i < 7; i++) {
            this.backProducts.push(res.Products[i]);
            if (res.Products[i].ordertype === 'req') {
              this.activeProducts.push(res.Products[i]);
            } else if (res.Products[i].ordertype === 'app') {
              this.closedProducts.push(res.Products[i]);
            }
          }
          console.log('this.backProducts');
          console.log(this.backProducts);
        } else {
          this.backProducts = res.Products;
          res.Products.forEach(product => {
            if (product.ordertype === 'req') {
              this.activeProducts.push(product);
            } else if (product.ordertype === 'app') {
              this.closedProducts.push(product);
            }
          });
          console.log('this.backProducts');
          console.log(this.backProducts);
        }
        this.backProducts[0].isActive = true;
      }
    });
  }

  clickEvent(product: any): void {
    this.backProducts.forEach(item => {
      item.isActive = false;
    });
    product.isActive = true;
    this.product = product;
    console.log('this.product');
    console.log(this.product);
  }
}
