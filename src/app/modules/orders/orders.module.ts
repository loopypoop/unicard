import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OrdersRouting} from './orders.routing';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import {SharedModule} from '../../shared/shared.module';



@NgModule({
  declarations: [OrderListComponent, OrderDetailsComponent],
  imports: [
    CommonModule,
    OrdersRouting,
    SharedModule
  ]
})
export class OrdersModule { }
