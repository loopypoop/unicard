import {Component, Input, OnInit, OnChanges} from '@angular/core';
import {Product} from '../../../shared/models/product';
import {ProductService} from '../../../shared/service/product.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DocumentService} from '../../../shared/service/document.service';
import {Document} from '../../../shared/models/Document';
import {Router} from '@angular/router';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit, OnChanges {

  @Input() reqId: number;
  product: Product;
  amountPayed = 2;
  userPhone = JSON.parse(localStorage.getItem('userPhone'));
  docs: Document[] = [];
  dots: string;
  // docs: Document[] = [
  //   {
  //     docid: '1',
  //     doctype: 'Договор'
  //   },
  //   {
  //     docid: '3',
  //     doctype: 'Согласие на сбор и обработку'
  //   }
  // ];

  error: string;
  listError: string;

  constructor(private productService: ProductService,
              private modalService: NgbModal,
              private documentService: DocumentService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.productService.getProductList(this.userPhone).subscribe(list => {
      if (list.Status === 0) {
        this.productService.getDetails(list.Products[0].req_id).subscribe(res => {
          this.reqId = list.Products[0].id;
          this.product = res.Data;
          if (this.product?.DatePayment && this.product?.DateClose) {
            this.product.DatePayment = this.convertDate(this.product.DatePayment);
            this.product.DateClose = this.convertDate(this.product.DateClose);
          }
        });
        this.documentService.getDocuments(list.Products[0].id).subscribe(res => {
          if (res.Status === 0) {
            this.docs = res.Documents;
            this.error = null;
          } else if (res.Status === 1) {
            this.error = res.StatusDesc;
          }
        });
      } else if (list.Status === 1) {
        this.listError = list.StatusDesc;
      }
    });
  }

  ngOnChanges() {
    this.product = null;
    this.listError = null;
    this.docs = [];
    this.productService.getDetails(this.reqId).subscribe(res => {
      if (res.Status == 0) {
        this.product = res.Data;
        if (this.product?.DatePayment && this.product?.DateClose) {
          this.product.DatePayment = this.convertDate(this.product.DatePayment);
          this.product.DateClose = this.convertDate(this.product.DateClose);
        }

        // if (!this.product?.SumPayment) {
        //   this.product.SumPayment = '0';
        // }
        this.documentService.getDocuments(this.reqId).subscribe(doc => {
          if (doc.Status === 0) {
            this.error = null;
            this.docs = doc.Documents;
          } else if (doc.Status === 1) {
            this.error = doc.StatusDesc;
          }
        });
      } else if (res.Status == 1) {
        this.listError = res.StatusDesc;
      }
    });
  }

  convertDate(dateString) {
    let p = dateString.split(/\D/g);
    return [p[2], p[1], p[0]].join('.');
  }

  openSuccessModal(content): void {
    this.modalService.open(content, {size: 'lg', centered: true});
  }

  openFile(docId: string) {
    this.documentService.getDocumentByDocId(docId).subscribe(res => {
      if (res && res.Status === 0) {
        window.open(res.FileUrl);
      }
    });
  }

  toPayment() {
    this.router.navigateByUrl('/payment/' + this.reqId);
  }
}
