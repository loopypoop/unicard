import {Component, Inject, OnInit} from '@angular/core';
import {SharedService} from '../../../shared/service/shared.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../shared/service/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  progressBar: number;
  loader: boolean;
  productId: number;
  product: any;

  constructor(@Inject(SharedService) private sharedService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private productService: ProductService) {
    this.productId = this.activatedRoute.snapshot.params['id'];
    this.productService.getProductById(this.productId).subscribe(res => {
      this.product = res.Data;
      if (this.product) {
        this.product.Price = parseInt(res.Data.Price);
      }
    });
  }

  ngOnInit(): void {
    this.sharedService.isLoading.subscribe(val => {
      this.loader = val;
    });
    this.sharedService.setInitial();
  }

  goNext(): void {
    this.sharedService.nextStep();
    this.router.navigateByUrl('auth/register/' + this.productId);
  }

}
