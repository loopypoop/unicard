import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product/product.component';
import {ProductRouting} from './product.routing';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [ProductComponent],
  imports: [
    CommonModule,
    ProductRouting,
    SharedModule
  ]
})
export class ProductModule { }
