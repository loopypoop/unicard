import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PaymentSuccessComponent} from './payment-success/payment-success.component';
import {PaymentFailComponent} from './payment-fail/payment-fail.component';
import {DeliveryAddressComponent} from './delivery-address/delivery-address.component';
import {WindowComponent} from './window/window.component';

const routes: Routes = [
  {
    path: 'success',
    component: PaymentSuccessComponent
  },
  {
    path: 'fail',
    component: PaymentFailComponent
  },
  {
    path: 'address/:reqId',
    component: DeliveryAddressComponent
  },
  {
    path: ':reqId',
    component: WindowComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRouting {}
