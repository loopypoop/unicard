import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PaymentRouting} from './payment.routing';
import {PaymentSuccessComponent} from './payment-success/payment-success.component';
import {PaymentFailComponent} from './payment-fail/payment-fail.component';
import { DeliveryAddressComponent } from './delivery-address/delivery-address.component';
import {FormsModule} from '@angular/forms';
import { WindowComponent } from './window/window.component';
import {SharedModule} from '../../shared/shared.module';



@NgModule({
  declarations: [PaymentSuccessComponent, PaymentFailComponent, DeliveryAddressComponent, WindowComponent],
    imports: [
        CommonModule,
        PaymentRouting,
        FormsModule,
        SharedModule
    ]
})
export class PaymentModule { }
