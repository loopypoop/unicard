import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DeliveryData} from '../../../shared/models/DeliveryData';
import {PaymentService} from '../../../shared/service/payment.service';

@Component({
  selector: 'app-delivery-address',
  templateUrl: './delivery-address.component.html',
  styleUrls: ['./delivery-address.component.scss']
})
export class DeliveryAddressComponent implements OnInit {

  isCityFocused = false;
  isStreetFocused = false;
  isHouseFocused = false;
  isPorchFocused = false;
  isFloorFocused = false;
  isApartmentFocused = false;
  reqId: number;
  error: string;

  issued: string;
  deliveryData: DeliveryData = new DeliveryData();

  constructor(private router: Router,
              private paymentService: PaymentService,
              private activatedRoute: ActivatedRoute) {
    this.reqId = this.activatedRoute.snapshot.params['reqId'];
  }

  ngOnInit(): void {
  }

  toNext(): void {
    this.deliveryData.IdProc = this.reqId.toString();
    console.log('here');
    if (!this.deliveryData.City || !this.deliveryData.Apartment || !this.deliveryData.Floor ||
    !this.deliveryData.House || !this.deliveryData.Porch || !this.deliveryData.Street) {
      this.error = 'Заполните обязательные поля!';
    } else {
      this.paymentService.sendDeliveryAddress(this.deliveryData).subscribe(res => {
        if (res.Status == 0) {
          console.log(res);
          this.router.navigateByUrl('/payment/success');
        } else if (res.Status == 1) {
          this.error = res.StatusDesc;
        }
      });
    }
  }
}
