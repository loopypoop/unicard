import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../shared/service/product.service';
import {SharedService} from '../../../shared/service/shared.service';
import {Product} from '../../../shared/models/product';
import {DocumentService} from '../../../shared/service/document.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-window',
  templateUrl: './window.component.html',
  styleUrls: ['./window.component.scss']
})
export class WindowComponent implements OnInit {

  payed = 1;
  reqId: number;
  error: string;
  product: Product;
  payLink: string;
  payError: string;
  graphId: string;

  @ViewChild('closebutton') modalClose;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private productService: ProductService,
              private documentService: DocumentService,
              @Inject(SharedService) private sharedService,) {
    this.reqId = parseInt(this.activatedRoute.snapshot.params['reqId']);
    console.log('this.reqId');
    console.log(this.reqId);
  }

  ngOnInit(): void {
    // this.getPayboxStatus();
    // if (this.graphId) {
    //   setInterval(() => this.getPayboxStatus(), 2000);
    // }
    this.productService.getDetails(this.reqId).subscribe(res => {
      console.log('res');
      console.log(res);
      this.product = res.Data;
      if (this.product?.DatePayment && this.product?.DateClose) {
        this.product.DatePayment = this.convertDate(this.product.DatePayment);
        this.product.DateClose = this.convertDate(this.product.DateClose);
      }
    });
  }

  convertDate(dateString) {
    let p = dateString.split(/\D/g);
    return [p[2], p[1], p[0]].join('.');
  }

  getPayLink() {
    const data = {
      Idproc: this.reqId.toString()
    };
    this.productService.getPayLink(data).subscribe(res => {
      console.log('pay pay pay pay');
      console.log(res);
      if (res.Status === 0) {
        this.payLink = res.Url;
        this.graphId = res.GraphId;
        setInterval(() => this.getPayboxStatus(), 3000);
      } else if (res.Status === 1) {
        this.payError = res.StatusDesc;
      }
    });
  }

  getPayboxStatus() {
    const val = {
      GraphId: this.graphId
    };
    this.productService.getPayStatus(val).subscribe(res => {
      console.log('in service');
      if (res.Status === 0) {
        this.modalClose.nativeElement.click();
        this.router.navigateByUrl('/orders');
      } else {
        console.log('status not 0');
      }
    });
  }

  close() {
    this.modalClose.nativeElement.click();
  }
}
