import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../shared/service/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-restore-password',
  templateUrl: './restore-password.component.html',
  styleUrls: ['./restore-password.component.scss']
})
export class RestorePasswordComponent implements OnInit {

  isPasswordVisible: boolean;
  newPassword: string;
  repeatPassword: string;
  taskId: string;
  errorStatus: string;

  constructor(private authService: AuthService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.taskId = this.activatedRoute.snapshot.params['taskId'];
  }

  ngOnInit(): void {
  }

  changePasswordVisibility(): void {
    this.isPasswordVisible = !this.isPasswordVisible;
  }

  restorePassword() {
    if (this.newPassword === this.repeatPassword && this.newPassword !== null) {
      const data = {
        "TaskId": this.taskId,
        "Data": {
          "NewPassword": this.newPassword
        }
      };
      this.authService.passwordReset(data).subscribe(res => {
        if (res.Status === 0) {
          this.router.navigateByUrl('/auth/login');
        } else if (res.Status === 0) {
          this.errorStatus = res.StatusDesc;
        }
      });
    }
  }

}
