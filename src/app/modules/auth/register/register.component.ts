import { Component, OnInit } from '@angular/core';
import {Application} from '../../../shared/models/application';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../shared/service/product.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  mainInfoEntered = false;
  isAgreed = false;
  errorDesc: string;

  application: Application = new Application();
  reqId: number;

  pipe = new DatePipe('en-US');

  constructor(private activatedRoute: ActivatedRoute,
              private productService: ProductService,
              private router: Router) {
    if (this.activatedRoute.snapshot.params['id']) {
      this.reqId = this.activatedRoute.snapshot.params['id'];
      this.productService.getProductById(this.reqId).subscribe(res => {
        this.application.Data.RequestID = this.reqId.toString();
        this.application.Data.IDN = res.Data.IDN;
        this.application.Data.Price = res.Data.Price;
        this.application.Data.VendorCode = res.Data.VendorCode;
        this.application.Data.Description = res.Data.Description;
      });
    }

  }

  ngOnInit(): void {
  }

  toNextForm(): void {
    this.mainInfoEntered = true;
  }

  openPersonalDataHandleFile() {
    window.open('assets/documents/Пользовательское согл окон.pdf');
  }

  openConfFile() {
    window.open('assets/documents/Политика конфиденциальности[34].pdf');
  }

  sendApplication() {
    this.application.Data.BirthDate = this.pipe.transform(this.application.Data.BirthDate, 'ddMMyyyy');
    this.application.Data.Cust_idexpire = this.pipe.transform(this.application.Data.Cust_idexpire, 'dd.MM.yyyy');
    this.application.Data.RequestDateTime = this.pipe.transform(Date.now(), 'ddMMyyyy');
    this.application.Data.PhoneNumber = '7' + this.application.Data.PhoneNumber;
    if (this.application.Data.IIN) {
      this.application.Data.IIN = this.application.Data.IIN.toString();
    } else {
      this.errorDesc = 'Введите ИИН!';
      this.application.Data.PhoneNumber = '';
    }
    if (this.application.Data.Cust_idno) {
      this.application.Data.Cust_idno = this.application.Data.Cust_idno.toString();
    } else {
      this.errorDesc = 'Введите номер удостоверения!';
      this.application.Data.PhoneNumber = '';
    }
    this.productService.sendApplication(this.application).subscribe(res => {
      if (res.Status === 0) {
        const url = '/confirmation/data/application/' + res.TaskId;
        localStorage.setItem('confirmationPhone', this.application.Data.PhoneNumber);
        localStorage.setItem('confirmationIIN', this.application.Data.IIN);
        this.router.navigateByUrl(url);
      } else if (res.Status === 1) {
        this.errorDesc = res.StatusDesc;
        this.application.Data.PhoneNumber = '';
      }
    });
  }

  agree() {
    this.isAgreed = !this.isAgreed;
  }

  back() {
    this.mainInfoEntered = false;
    this.isAgreed = !this.isAgreed;
  }

  isDigitPhone(): void {
    document.getElementById("phone").addEventListener("keypress", function (evt) {
      if (evt.which < 48 || evt.which > 57)
      {
        evt.preventDefault();
      }
    });
  }
  isDigitIIN(): void {
    document.getElementById("iin").addEventListener("keypress", function (evt) {
      if (evt.which < 48 || evt.which > 57)
      {
        evt.preventDefault();
      }
    });
  }
  isDigitIDNo(): void {
    document.getElementById("idNumber").addEventListener("keypress", function (evt) {
      if (evt.which < 48 || evt.which > 57)
      {
        evt.preventDefault();
      }
    });
  }
}
