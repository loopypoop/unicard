import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import {SettingsRouting} from './settings.routing';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    SettingsRouting,
    FormsModule
  ]
})
export class SettingsModule { }
