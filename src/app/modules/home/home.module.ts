import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './page/home.component';
import {AdvantagesComponent} from './page/advantages/advantages.component';
import {HomeBannerComponent} from './page/home-banner/home-banner.component';
import {PartnersComponent} from './page/partners/partners.component';
import {InstructionComponent} from './page/instruction/instruction.component';
import {SharedModule} from '../../shared/shared.module';
import {HomeRoutingModule} from './home.routing';



@NgModule({
  declarations: [HomeComponent, AdvantagesComponent, HomeBannerComponent, PartnersComponent, InstructionComponent],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
