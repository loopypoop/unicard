import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit {

  activeSection: string;

  constructor() { }

  ngOnInit(): void {
  }

}
