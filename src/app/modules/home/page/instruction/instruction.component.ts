import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-instruction',
  templateUrl: './instruction.component.html',
  styleUrls: ['./instruction.component.scss']
})
export class InstructionComponent implements OnInit {

  instructionStep = 1;

  constructor() { }

  ngOnInit(): void {
  }

  stepOne() {
    this.instructionStep = 1;
  }

  stepTwo() {
    this.instructionStep = 2;
  }

  stepThree() {
    this.instructionStep = 3;
  }

  stepFour() {
    this.instructionStep = 4;
  }

}
