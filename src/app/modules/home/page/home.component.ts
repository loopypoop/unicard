import {Component, Inject, OnInit} from '@angular/core';
import {SharedService} from '../../../shared/service/shared.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  toPartner: boolean;
  activeSection: string;

  constructor(@Inject(SharedService) private sharedService) { }

  ngOnInit(): void {
    this.sharedService.toPartners.subscribe(val => {
      if (val) {
        this.scrollDown('partners');
        this.sharedService.noScrollPartners();
      }
    });
  }

  scrollDown(divName: string): void {
    this.activeSection = divName;
    document.getElementById(divName).scrollIntoView();
  }

}
