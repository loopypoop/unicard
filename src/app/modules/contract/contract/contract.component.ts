import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DocumentService} from '../../../shared/service/document.service';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit {
  isAgree = false;
  reqId: number;

  constructor(private router: Router,
              private documentService: DocumentService,
              private activatedRouter: ActivatedRoute) {
    this.reqId = this.activatedRouter.snapshot.params['reqId'];
  }

  ngOnInit(): void {
  }

  sign(): void {
    const data = {
      "Data": {
        "IdProc": this.reqId
      }
    };
    this.documentService.signContract(data).subscribe(res => {
      console.log(res);
      if (res && res.Status === 0) {
        this.router.navigateByUrl('/confirmation/contract/' + this.reqId + '/' + res.TaskId);
      }
    });
  }

  agree() {
    this.isAgree = !this.isAgree;
  }

  openPersonalDataHandleFile() {
    window.open('assets/documents/Согласие_на_обработку_персональных_данных.pdf');
  }

  openConfFile() {
    window.open('assets/documents/Политика конфиденциальности[34].pdf');
  }
}
