import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractRoutingModule } from './contract-routing.module';
import { ContractComponent } from './contract/contract.component';
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [ContractComponent],
    imports: [
        CommonModule,
        ContractRoutingModule,
        FormsModule
    ]
})
export class ContractModule { }
