import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayboxRoutingModule } from './paybox-routing.module';
import { PayboxComponent } from './paybox/paybox.component';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [PayboxComponent],
  imports: [
    CommonModule,
    PayboxRoutingModule,
    SharedModule
  ]
})
export class PayboxModule { }
