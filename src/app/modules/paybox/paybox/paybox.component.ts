import {DOCUMENT} from '@angular/common';
import {Component, Inject, OnInit, Renderer2} from '@angular/core';

declare function call(): any;

@Component({
  selector: 'app-paybox',
  templateUrl: './paybox.component.html',
  styleUrls: ['./paybox.component.scss']
})
export class PayboxComponent implements OnInit {

  constructor(@Inject(DOCUMENT) private document: Document,
              private renderer2: Renderer2) {

  }

  ngOnInit(): void {
  }

  click() {
    const textScript = this.renderer2.createElement('script');
    textScript.src = 'https://code.jquery.com/jquery-3.3.1.slim.min.js';
    this.renderer2.appendChild(this.document.body, textScript);

    const srcScript = this.renderer2.createElement('script');
    srcScript.type = 'text/javascript';
    srcScript.text = `
      (function (p, a, y, b, o, x) {
        console.log("opened");
        o = p.createElement(a);
        x = p.getElementsByTagName(a)[0];
        o.async = 1;
        o.src = 'https://widget.paybox.money/v1/paybox/pbwidget.js?' + 1 * new Date();
        x.parentNode.insertBefore(o, x);
      })(document, 'script');
    `;
    this.renderer2.appendChild(this.document.body, srcScript);
  }
}




