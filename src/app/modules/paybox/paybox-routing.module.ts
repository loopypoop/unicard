import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PayboxComponent} from './paybox/paybox.component';

const routes: Routes = [
  {
    path: '',
    component: PayboxComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayboxRoutingModule { }
