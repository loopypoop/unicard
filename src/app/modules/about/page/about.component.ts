import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  styles: [`
    .modal-content {
      background-color: transparent;
      border: 0;
    }
  `]
})
export class AboutComponent implements OnInit {

  fullName: string;
  email: string;
  phone: string;
  bin: string;
  companyName: string;

  townList = ['Алматы', 'Нур-Султан', 'Шымкент', 'Тараз', 'Павлодар', 'Караганды'];
  itemList = ['Одежда', 'Красота и здоровье', 'Детские товары', 'Ремонт/Строительство',
    'Образование', 'Продукты питания', 'Ювелирные изделия', 'Флористика', 'Электроника', 'Товары для дома'];

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  openVerticallyCentered(content): void {
    this.modalService.open(content, { centered: true });
  }

  openSuccessModal(content): void {
    this.modalService.open(content, {size: 'sm', centered: true});
  }

}
