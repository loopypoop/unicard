import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InstallmentWindowComponent} from './installment-window/installment-window.component';
import {InstallmentDecisionComponent} from './installment-decision/installment-decision.component';

const routes: Routes = [
  {
    path: '',
    component: InstallmentWindowComponent
  },
  {
    path: 'decision',
    component: InstallmentDecisionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstallmentRouting {}
