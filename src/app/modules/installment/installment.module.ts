import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InstallmentRouting} from './installment.routing';
import {SharedModule} from '../../shared/shared.module';
import { InstallmentWindowComponent } from './installment-window/installment-window.component';
import { InstallmentDecisionComponent } from './installment-decision/installment-decision.component';



@NgModule({
  declarations: [InstallmentWindowComponent, InstallmentDecisionComponent],
  imports: [
    CommonModule,
    InstallmentRouting,
    SharedModule
  ]
})
export class InstallmentModule { }
