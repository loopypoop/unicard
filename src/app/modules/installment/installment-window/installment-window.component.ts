import {Component, Inject, OnInit} from '@angular/core';
import {SharedService} from '../../../shared/service/shared.service';

@Component({
  selector: 'app-installment-window',
  templateUrl: './installment-window.component.html',
  styleUrls: ['./installment-window.component.scss']
})
export class InstallmentWindowComponent implements OnInit {

  isLoading = false;

  constructor(@Inject(SharedService) private sharedService) {}

  ngOnInit(): void {
  }

  openLoader(): void {
    this.sharedService.startLoader();
    this.isLoading = true;
  }

}
