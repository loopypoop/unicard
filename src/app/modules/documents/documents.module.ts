import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentListComponent } from './document-list/document-list.component';
import {DocumentsRouting} from './documents.routing';



@NgModule({
  declarations: [DocumentListComponent],
  imports: [
    CommonModule,
    DocumentsRouting
  ]
})
export class DocumentsModule { }
