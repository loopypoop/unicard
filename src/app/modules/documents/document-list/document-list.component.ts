import {Component, OnInit} from '@angular/core';
import {Document} from '../../../shared/models/Document';
import {DocumentService} from '../../../shared/service/document.service';

// @ts-ignore
@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit {

  documents: Document[] = [];

  constructor(private documentService: DocumentService) {
  }

  ngOnInit(): void {}

  openFile(DocId: string) {
    this.documentService.getDocumentByDocId(DocId).subscribe(res => {
      if (res && res.Status === 0) {
        window.open(res.FileUrl);
      }
    });
  }

  openFile_1() {
    window.open('assets/documents/Согласие_на_обработку_персональных_данных.pdf');
  }

  openFile_2() {
    window.open('assets/documents/Согласие на выдачу отчета.pdf');
  }

  openFile_3() {
    window.open('assets/documents/Согласие_на_предоставление_инф_в_пкб.pdf');
  }

  openFile_4() {
    window.open('assets/documents/Договор оказания услуг окон.pdf');
  }

  openFile_5() {
    window.open('assets/documents/Пользовательское согл Unicard 1.pdf');
  }

}
