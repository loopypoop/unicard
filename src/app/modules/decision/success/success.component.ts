import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DocumentService} from '../../../shared/service/document.service';
import {ProductService} from '../../../shared/service/product.service';
import {SharedService} from '../../../shared/service/shared.service';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {

  payed = 1;
  reqId: number;
  taskId: string;
  price: number;
  sumPayment: number;
  term: string;
  description: string;
  datePayment: string;
  document: string;
  error: string;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private documentService: DocumentService,
              private productService: ProductService,
              @Inject(SharedService) private sharedService) {
    this.reqId = this.activatedRoute.snapshot.params['reqId'];
  }

  ngOnInit(): void {
    this.sharedService.lastStep();
    this.productService.getInstallmentApprovalStatus(this.reqId).subscribe(res => {
      this.price = parseInt(res.Price);
      this.description = res.Description;
      this.sumPayment = parseInt(res.Sumpayment);
      this.term = res.Term;
      this.document = res.Dogovor;
      this.datePayment = res.Datepayment;
      this.datePayment = this.convertDate(this.datePayment);
    });
  }

  toContract() {
    const data = {
      'Data': {
        'IdProc': this.reqId
      }
    };

    localStorage.setItem('IdProc', this.reqId.toString());
    this.documentService.signContract(data).subscribe(res => {
      console.log(res);
      if (res && res.Status === 0) {
        this.router.navigateByUrl('/confirmation/contract/' + this.reqId + '/' + res.TaskId);
      } else if (res && res.Status === 1) {
        this.error = res.StatusDesc;
      }
    });
    this.sharedService.nextStep();
  }

  openFile() {
    window.open(this.document);
  }

  convertDate(dateString) {
    let p = dateString.split(/\D/g);
    return [p[2], p[1], p[0]].join('.');
  }
}
