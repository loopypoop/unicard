import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuccessComponent } from './success/success.component';
import { FailComponent } from './fail/fail.component';
import {DecisionRouting} from './decision.routing';
import {SharedModule} from '../../shared/shared.module';



@NgModule({
  declarations: [SuccessComponent, FailComponent],
  imports: [
    CommonModule,
    DecisionRouting,
    SharedModule
  ]
})
export class DecisionModule { }
