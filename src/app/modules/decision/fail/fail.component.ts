import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../../shared/service/product.service';

@Component({
  selector: 'app-fail',
  templateUrl: './fail.component.html',
  styleUrls: ['./fail.component.scss']
})
export class FailComponent implements OnInit {

  payed = 1;
  reqId: number;
  bin: string;
  description: string;
  vendorCode: string;
  price: number;

  constructor(private activatedRoute: ActivatedRoute,
              private productService: ProductService) {
    this.reqId = this.activatedRoute.snapshot.params['reqId'];
  }

  ngOnInit(): void {
    this.productService.getProductById(this.reqId).subscribe(res => {
      console.log(res.Data);
      this.bin = res.Data.IDN;
      this.vendorCode = res.Data.VendorCode;
      this.price = parseInt(res.Data.Price);
      this.description = res.Data.Description;
    });
  }

}
