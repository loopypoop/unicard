import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FailComponent} from './fail/fail.component';
import {SuccessComponent} from './success/success.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'success/:reqId',
        component: SuccessComponent
      },
      {
        path: 'fail/:reqId',
        component: FailComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DecisionRouting {}
