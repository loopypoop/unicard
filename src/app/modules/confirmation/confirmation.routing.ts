import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DataConfirmationComponent} from './data-confirmation/data-confirmation.component';
import {ContractConfirmationComponent} from './contract-confirmation/contract-confirmation.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'data/:purpose/:taskId',
        component: DataConfirmationComponent
      },
      {
        path: 'contract/:reqId/:taskId',
        component: ContractConfirmationComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfirmationRouting {}
