import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataConfirmationComponent } from './data-confirmation/data-confirmation.component';
import { ContractConfirmationComponent } from './contract-confirmation/contract-confirmation.component';
import {ConfirmationRouting} from './confirmation.routing';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [DataConfirmationComponent, ContractConfirmationComponent],
    imports: [
        ConfirmationRouting,
        CommonModule,
        SharedModule,
        FormsModule
    ]
})
export class ConfirmationModule { }
