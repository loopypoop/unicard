import {Component, OnInit} from '@angular/core';
import {DocumentService} from '../../../shared/service/document.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../shared/service/product.service';

@Component({
  selector: 'app-contract-confirmation',
  templateUrl: './contract-confirmation.component.html',
  styleUrls: ['./contract-confirmation.component.scss']
})
export class ContractConfirmationComponent implements OnInit {

  showErrorAlert: boolean;
  activeInput: number;
  isLoading = false;
  taskId: string;
  reqId: number;

  dOne;
  dTwo;
  dThree;
  dFour;
  dFive;
  dSix;
  confirmationCode: string;
  counter = 30;
  phoneNum: string;
  userPhone: string;
  userIin: string;
  error: string;
  hasError: any;

  constructor(private router: Router,
              private documentService: DocumentService,
              private activatedRoute: ActivatedRoute,
              private productService: ProductService) {
    this.taskId = this.activatedRoute.snapshot.params['taskId'];
    this.reqId = this.activatedRoute.snapshot.params['reqId'];
    this.hasError = this.activatedRoute.snapshot.paramMap.get('hasError');

    this.userIin = localStorage.getItem('confirmationIIN');
    this.userPhone = localStorage.getItem('confirmationPhone');
  }

  ngOnInit() {
    this.activeInput = 1;
    this.showErrorAlert = false;

    if (localStorage.getItem('confirmationPhone')) {
      this.phoneNum = localStorage.getItem('confirmationPhone');
    }

    if (this.hasError) {
      this.error = this.hasError;
      this.showErrorAlert = true;
      console.log('typeof this.showErrorAlert');
      console.log(typeof this.showErrorAlert);
      console.log(this.showErrorAlert);
    }

    this.smsCountDown();
  }

  async smsCountDown() {
    while (this.counter !== 0) {
      this.counter--;
      await new Promise<void>(done => setTimeout(() => done(), 1000));
    }
    if (this.counter === 0) {
      console.log('RESTART');
    }
  }

  delay(n) {
    n = n || 2000;
    return new Promise<void>(done => {
      setTimeout(() => {
        done();
      }, n);
    });
  }

  clear() {
    this.dOne = null;
    this.dTwo = null;
    this.dThree = null;
    this.dFour = null;
    this.dFive = null;
    this.dSix = null;
    this.confirmationCode = null;
    this.counter = 30;
  }

  resend() {
    this.productService.resendSms(this.userIin, this.userPhone).subscribe(res => {
      this.clear();
      this.error = 'Код переотправлен!';
      this.showErrorAlert = true;
    });
  }

  hideErrorAlert(): void {
    this.showErrorAlert = false;
  }

  onCodeChange(lastNum: string): void {
    this.confirmationCode = this.dOne + this.dTwo + this.dThree + this.dFour + this.dFive + lastNum;
    if (this.taskId) {
      const data = {
        "TaskId": this.taskId,
        "Data": {
          "SMSCode": this.confirmationCode
        }
      };
      this.documentService.signContract(data).subscribe(res => {
        console.log(res);
        if (res && res.Status === 0) {
          this.router.navigateByUrl('/payment/' + this.reqId);
        } else if (res && res.Status === 1) {
          this.taskId = res.TaskId;
          this.error = res.StatusDesc;
          this.router.navigate(['/confirmation/contract/' + this.reqId + '/' + this.taskId, {hasError: this.error}]).then(() => {
            window.location.reload();
            this.clear();
          });
        }
      });
    }
  }
}
