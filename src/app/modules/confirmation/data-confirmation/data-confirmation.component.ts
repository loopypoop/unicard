import {Component, Inject, OnInit} from '@angular/core';
import {AuthService} from '../../../shared/service/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../shared/service/product.service';
import {SharedService} from '../../../shared/service/shared.service';
import {delay} from 'rxjs/operators';

@Component({
  selector: 'app-data-confirmation',
  templateUrl: './data-confirmation.component.html',
  styleUrls: ['./data-confirmation.component.scss']
})
export class DataConfirmationComponent implements OnInit {

  showErrorAlert = false;
  activeInput: number;
  isLoading = false;
  dOne;
  dTwo;
  dThree;
  dFour;
  dFive;
  dSix;
  confirmationCode: string;
  taskId: string;
  purpose: string;
  phoneNum: string;
  counter = 30;
  error: string;
  hasError: any;
  userPhone: string;
  userIin: string;
  resendAlert: boolean;

  constructor(private router: Router,
              private authService: AuthService,
              private activeRoute: ActivatedRoute,
              private productService: ProductService,
              @Inject(SharedService) private sharedService) {
    this.purpose = this.activeRoute.snapshot.params['purpose'];
    this.taskId = this.activeRoute.snapshot.params['taskId'];
    this.hasError = this.activeRoute.snapshot.paramMap.get('hasError');

    this.userIin = localStorage.getItem('confirmationIIN');
    this.userPhone = localStorage.getItem('confirmationPhone');
  }

  ngOnInit() {
    this.activeInput = 1;
    this.showErrorAlert = false;
    // console.log(this.phoneNum = localStorage.getItem('confirmationPhone'));
    if (localStorage.getItem('confirmationPhone')) {
      this.phoneNum = localStorage.getItem('confirmationPhone');
    }


    if (this.hasError && this.hasError != null) {
      this.error = this.hasError;
      this.showErrorAlert = true;
      console.log('typeof this.showErrorAlert');
      console.log(typeof this.showErrorAlert);
      console.log(this.showErrorAlert);
    }

    this.smsCountDown();
  }

  async smsCountDown() {
    while (this.counter !== 0) {
      this.counter--;
      await new Promise<void>(done => setTimeout(() => done(), 1000));
    }
    if (this.counter === 0) {
      console.log('RESTART');
    }
  }

  delay(n) {
    n = n || 2000;
    return new Promise<void>(done => {
      setTimeout(() => {
        done();
      }, n);
    });
  }

  hideErrorAlert(): void {
    this.showErrorAlert = false;
  }

  clear() {
    this.dOne = null;
    this.dTwo = null;
    this.dThree = null;
    this.dFour = null;
    this.dFive = null;
    this.dSix = null;
    this.confirmationCode = null;
    this.counter = 30;
  }

  resend() {
    this.productService.resendSms(this.userIin, this.userPhone).subscribe(res => {
      this.clear();
      this.error = 'Код переотправлен!';
      this.showErrorAlert = true;
    });
  }

  onCodeChange(lastNum: string): void {
    this.confirmationCode = this.dOne + this.dTwo + this.dThree + this.dFour + this.dFive + lastNum;
    if (this.purpose && this.purpose === 'restore') {
      if (this.taskId) {
        const data = {
          "TaskId": this.taskId,
          "Data": {
            "SMSCode": this.confirmationCode
          }
        };
        this.authService.passwordReset(data).subscribe(res => {
          console.log(res);
          // изменить статус на 0
          if (res && res.Status === 0) {
            this.router.navigateByUrl('/auth/restore/' + res.TaskId);
          } else if (res.Status === 1) {
            this.taskId = res.TaskId;
            this.error = res.StatusDesc;
            this.router.navigate(['/confirmation/data/restore/' + this.taskId, {hasError: this.error}]).then(() => {
              window.location.reload();
              this.clear();
            });

          }
        });
      }
    } else if (this.purpose && this.purpose === 'application') {
      if (this.taskId) {
        const data = {
          'TaskId': this.taskId,
          'Data': {
            'SMSCode': this.confirmationCode
          }
        };
        this.productService.confirmApplication(data).subscribe(res => {
          console.log('res after sms', res);
          // change status to 0
          if (res && res.Status === 0) {
            // open loader and send request to get installment status
            this.sharedService.startLoader();
            this.isLoading = true;
            this.sharedService.nextStep();

            this.productService.getInstallmentApprovalStatus(res.Id).subscribe(val => {
              console.log('applicaction approvement result=', val);
              let that = this;
              setTimeout(function () {
                if (val.Status == -1) {
                  that.router.navigateByUrl('/decision/fail/' + res.Id);
                } else {
                  that.router.navigateByUrl('/decision/success/' + res.Id);
                }
                that.sharedService.stopLoader();
                that.isLoading = false;
              }, 3000);
            });
          } else if (res && res.Status === 1) {
            this.taskId = res.TaskId;
            this.error = res.StatusDesc;
            console.log('this.error');
            console.log(this.error);
            this.router.navigate(['/confirmation/data/application/' + this.taskId, {hasError: this.error}]).then(() => {
              window.location.reload();
              this.clear();
            });
          }
        });
      }
    }
  }
}
