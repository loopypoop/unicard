import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confidentiality',
  templateUrl: './confidentiality.component.html',
  styleUrls: ['./confidentiality.component.scss']
})
export class ConfidentialityComponent implements OnInit {
  activeSection: string;

  constructor() { }

  ngOnInit(): void {
  }

  scroll(divName: string): void {
    this.activeSection = divName;
    document.getElementById(divName).scrollIntoView();
  }
}
