import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ConfidentialityRouting} from './confidentiality.routing';
import { ConfidentialityComponent } from './confidentiality/confidentiality.component';



@NgModule({
  declarations: [ConfidentialityComponent],
  imports: [
    CommonModule,
    ConfidentialityRouting
  ]
})
export class ConfidentialityModule { }
