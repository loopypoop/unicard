import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringPrice'
})
export class StringPricePipe implements PipeTransform {

  transform(value: string, args?: any): string {
    let val = parseInt(value);
    return val.toLocaleString();
  }

}
