import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {


  constructor(private http: HttpClient) { }

  getDocuments(IdProc: number): Observable<any> {
    return this.http.get(`/restapi/services/run/ucard_requisition_docs?IdProc=${IdProc}`);
  }

  getDocumentByDocId(DocId: string): Observable<any> {
    return this.http.get(`/restapi/services/run/ucard_doc?DocId=${DocId}`);
  }

  signContract(data: any): Observable<any> {
    return this.http.post(`/restapi/services/run/ucard_contract_signing`, data);
  }

  test(IdProc: number): Observable<any> {
    return this.http.get(`test?IdProc${IdProc}`);
  }

}
