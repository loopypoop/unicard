import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {LoginData} from '../models/loginData';
import {PasswordChangeData} from '../models/passwordChangeData';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn = false;
  redirectUrl: string;

  constructor(private http: HttpClient, private route: Router) { }

  passwordChange(passwordChangeData: PasswordChangeData): Observable<any> {
    return this.http.post(`/restapi/services/run/ucard_passwordchange`, passwordChangeData);
  }

  login(loginData: LoginData): Observable<any> {
    return this.http.post(`/restapi/services/run/ucard_auth`, loginData).pipe(tap(val =>
      val.StatusDesc === 'Успешно' ? (this.isLoggedIn = true) : this.isLoggedIn = false));
  }

  passwordReset(passwordResetData: any): Observable<any> {
    console.log('service ', passwordResetData);
    return this.http.post(`/restapi/services/run/ucard_passwordreset`, passwordResetData);
  }

  logout(): false {
    this.resetLocalVariables();
    window.location.reload();
    return false;
  }

  private resetLocalVariables(): void {
    localStorage.removeItem('userId');
    localStorage.removeItem('userPhone');
    localStorage.removeItem('confirmationPhone');
    localStorage.removeItem('user');
    localStorage.removeItem('confirmationIIN');
  }
}
