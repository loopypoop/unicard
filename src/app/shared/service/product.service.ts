import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {}

    getProductById(id: number): Observable<any> {
    return this.http.get(`/restapi/services/run/ucard_application_info?id=${id}`, {responseType: 'json'});
  }

  sendApplication(application: any): Observable<any> {
    return this.http.post(`/restapi/services/run/ucard_installment_reg`, application);
  }

  confirmApplication(data: any): Observable<any> {
    return this.http.post(`/restapi/services/run/ucard_installment_reg`, data);
  }

  getInstallmentApprovalStatus(idProc): Observable<any> {
    return this.http.get(`/restapi/services/run/ucard_requisition_info?IDproc=${idProc}`);
  }

  getProductList(phoneNum: string): Observable<any> {
    return this.http.get(`/restapi/services/run/ucard_customer_orders?Phonenumber=${phoneNum}`);
  }

  getDetails(reqId: number): Observable<any> {
    return this.http.get(`/restapi/services/run/ucard_requisition_detail?Idproc=${reqId}`);
  }

  resendSms(iin: string, phone: string) {
    return this.http.get(`/restapi/services/run/ucard_resms_ident?IIN=${iin}&Phonenumber=${phone}`);
  }

  getPayLink(Idproc): Observable<any> {
    return this.http.post(`/restapi/services/run/ucard_pay_init`, Idproc);
  }

  getPayStatus(GraphId): Observable<any> {
    return this.http.post(`/restapi/services/run/ucard_pay_status`, GraphId);
  }
}
