import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  private loadingStream = new BehaviorSubject<boolean>(false);
  private progressStream = new BehaviorSubject<number>(0);
  private toPartnerStream = new BehaviorSubject<boolean>(false);
  isLoading = this.loadingStream.asObservable();
  progressBar = this.progressStream.asObservable();
  toPartners = this.toPartnerStream.asObservable();

  constructor() { }

  startLoader(): void {
    this.loadingStream.next(true);
  }

  stopLoader(): void {
    this.loadingStream.next(false);
  }

  setInitial(): void {
    this.progressStream.next(1);
  }

  nextStep(): void {
    this.progressStream.next(this.progressStream.value + 1);
  }

  skipStep(): void {
    this.progressStream.next(this.progressStream.value + 2);
  }

  cleanProgress() {
    this.progressStream.next(100);
  }

  lastStep() {
    this.progressStream.next(4);
  }

  scrollPartners() {
    this.toPartnerStream.next(true);
  }

  noScrollPartners() {
    this.toPartnerStream.next(false);
  }
}
