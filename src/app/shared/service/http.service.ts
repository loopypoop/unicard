import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  sendApplication(application: any): Observable<any> {
    return this.http.post(`/restapi/services/run/ucard_installment_reg`, application);
  }

  sendSmsCode(smsCode: any): Observable<any> {
    return this.http.post(`/restapi/services/run/ucard_installment_reg`, smsCode);
  }

  installmentStatus(): Observable<any> {
    return this.http.get(`/restapi/services/run/ucard_requisition_info?IDproc=1`);
  }

  businessApplication(application: any): Observable<any> {
    return this.http.post('/restapi/services/run/ucard_business', application);
  }
}
