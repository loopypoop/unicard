import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DeliveryData} from "../models/DeliveryData";

@Injectable({
  providedIn: 'root'
})
export class PaymentService {


  constructor(private http: HttpClient) { }

  sendDeliveryAddress(deliveryData: DeliveryData): Observable<any> {
    return this.http.post(`/restapi/services/run/ucard_customer_deliveryfill`, deliveryData);
  }

  test(IdProc: number): Observable<any> {
    return this.http.get(`test?IdProc${IdProc}`);
  }

}
