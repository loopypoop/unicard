import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  defaultTrue = true;
  defaultFalse = false;
  firstQ = true;

  constructor() { }

  ngOnInit(): void {
  }

  clickFirstQuestion(): void {
    this.firstQ = !this.firstQ;
  }
}
