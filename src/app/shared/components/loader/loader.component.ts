import { Component, OnInit } from '@angular/core';
import {SharedService} from '../../service/shared.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  constructor(private sharedService: SharedService) { }

  ngOnInit(): void {
  }

  stopLoader(): void {
    this.sharedService.stopLoader();
  }

}
