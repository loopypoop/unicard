import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoaderComponent} from './components/loader/loader.component';
import {NumberFormatPipe} from './pipes/number-format.pipe';
import {FaqComponent} from './components/faq/faq.component';
import { StringPricePipe } from './pipes/string-price.pipe';
import { SafePipe } from './pipes/safe.pipe';


@NgModule({
  declarations: [LoaderComponent, NumberFormatPipe, FaqComponent, StringPricePipe, SafePipe],
  exports: [
    NumberFormatPipe,
    LoaderComponent,
    FaqComponent,
    StringPricePipe,
    SafePipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule {
}
