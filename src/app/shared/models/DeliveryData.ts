export class DeliveryData {
  IdProc: string;
  City: string;
  Street: string;
  House: string;
  Porch: string;
  Floor: string;
  Apartment: string;

  constructor() {
  }
}
