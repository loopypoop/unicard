export class ProductDTO {
  id: number;
  ordertype: string;
  description: string;
  nameseller: string;
  price: string;
  statusproduct: string;
  vendorcode: string;
  isActive = false;

  constructor() { }
}
