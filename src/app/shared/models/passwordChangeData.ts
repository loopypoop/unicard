export class PasswordChangeData {
  PhoneNumber: string;
  OldPassword: string;
  NewPassword: string;

  constructor() {
  }
}
