export class Product {
  DateClose: string;
  DatePayment: string;
  Description: string;
  NameSeller: string;
  Price: string;
  Statusproduct: string;
  SumPayment: string;
  Term: string;
  VendorCode: string;

  constructor() {}
}
