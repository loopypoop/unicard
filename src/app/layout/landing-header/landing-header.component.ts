import {Component, Inject, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../shared/service/auth.service';
import {SharedService} from '../../shared/service/shared.service';

@Component({
  selector: 'app-landing-header',
  templateUrl: './landing-header.component.html',
  styleUrls: ['./landing-header.component.scss']
})
export class LandingHeaderComponent implements OnInit {

  isAuthenticated: boolean;
  activeSection: string;
  toPartners: boolean;

  constructor(private router: Router,
              private authService: AuthService,
              @Inject(SharedService) private sharedService) { }

  ngOnInit(): void {
    console.log('userId', localStorage.getItem('userId'));
    this.isAuthenticated = localStorage.getItem('userId') !== null;
    if (this.toPartners) {
      this.scrollDown('partners');
      this.toPartners = false;
      this.sharedService.noScrollPartners();
    }
  }

  scrollDown(divName: string): void {
    this.activeSection = divName;
    document.getElementById(divName).scrollIntoView();
  }

  logout() {
    this.isAuthenticated = this.authService.logout();
  }

  partnersNav() {
    this.sharedService.scrollPartners();
    this.toPartners = true;
  }
}
