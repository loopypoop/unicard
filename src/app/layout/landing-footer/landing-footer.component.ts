import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-footer',
  templateUrl: './landing-footer.component.html',
  styleUrls: ['./landing-footer.component.scss']
})
export class LandingFooterComponent implements OnInit {

  activeSection: string;

  constructor() { }

  ngOnInit(): void {
  }

  openConfFile() {
    window.open('assets/documents/Политика конфиденциальности[34].pdf');
  }

  openOfferFile() {
    window.open('assets/documents/Договор оферты v2.pdf');
  }

  scroll(divName: string): void {
    this.activeSection = divName;
    document.getElementById(divName).scrollIntoView();
  }
}
