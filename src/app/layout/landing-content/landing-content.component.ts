import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-content',
  templateUrl: './landing-content.component.html',
  styleUrls: ['./landing-content.component.scss']
})
export class LandingContentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onActivate(event): void {
    window.scroll(0, 0);
  }

}
