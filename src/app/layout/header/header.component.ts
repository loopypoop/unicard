import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedService} from '../../shared/service/shared.service';
import {AuthService} from '../../shared/service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isAuthenticated: boolean;
  route: string;
  loader: boolean;
  progressBar: number;
  userId: number;

  constructor(private router: Router,
              @Inject(SharedService) private sharedService,
              private authService: AuthService) {
    this.route = this.router.url;
  }

  ngOnInit(): void {
    this.sharedService.isLoading.subscribe(val => {
      this.loader = val;
      this.sharedService.progressBar.subscribe(progress => {
        this.progressBar = progress;
      });
    });

    this.isAuthenticated = localStorage.getItem('userId') !== null;
  }

  logout() {
    this.isAuthenticated = this.authService.logout();
    this.sharedService.cleanProgress();
  }

  toLogin() {
    this.sharedService.cleanProgress();
    this.router.navigateByUrl('/auth/login');
  }
}
