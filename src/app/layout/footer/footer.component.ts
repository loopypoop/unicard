import {Component, Inject, OnInit} from '@angular/core';
import {SharedService} from '../../shared/service/shared.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  loader;
  constructor(@Inject(SharedService) private sharedService) { }

  ngOnInit(): void {
    this.sharedService.isLoading.subscribe(val => {
      this.loader = val;
    });
  }

  openConfFile() {
    window.open('assets/documents/Политика конфиденциальности[34].pdf');
  }

  openOfferFile() {
    window.open('assets/documents/Договор оферты v2.pdf');
  }
}
